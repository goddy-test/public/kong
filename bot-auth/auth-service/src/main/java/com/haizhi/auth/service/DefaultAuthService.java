package com.haizhi.auth.service;

import com.haizhi.auth.config.KongAuthConfig;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import javax.mail.internet.MimeMessage;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.login.LoginException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/19.
 */
@Slf4j
@Service
public class DefaultAuthService implements AuthService {

  @Value("${kong.auth.api.url}")
  private String kongApiUrl;

  @Value("${kong.auth.plugin.host}")
  private String kongPluginHost;

  @Value("${spring.mail.username}")
  private String sender;

  private final JavaMailSender javaMailSender;

  private final ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

  private final KongAuthConfig kongAuthConfig;

  private final UserService userService;

  private final TrustManager[] trustManagers;

  private final String REFRESH_GRANT_TYPE = "refresh_token";
  private final String PASSWORD_FORMAT = "grant_type=%s&client_id=%s&client_secret=%s&username=%s&password=%s&authenticated_userid=%s&scope=%s&provision_key=%s";
  private final String REFRESH_FORMAT = "grant_type=%s&client_id=%s&client_secret=%s&refresh_token=%s";

  public DefaultAuthService(JavaMailSender javaMailSender,
      ReactiveRedisTemplate<String, String> reactiveRedisTemplate,
      KongAuthConfig kongAuthConfig, UserService userService) {
    this.javaMailSender = javaMailSender;
    this.reactiveRedisTemplate = reactiveRedisTemplate;
    this.kongAuthConfig = kongAuthConfig;
    this.userService = userService;
    X509TrustManager xtm = new X509TrustManager() {
      @Override
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      @Override
      public void checkServerTrusted(X509Certificate[] arg0, String arg1)
          throws CertificateException {
      }

      @Override
      public void checkClientTrusted(X509Certificate[] arg0, String arg1)
          throws CertificateException {
      }
    };
    trustManagers = new TrustManager[]{xtm};
  }

  @Override
  public Mono<String> sendEmailCode(String email) {
    String verifyCode = String.format("%6s", RandomUtils.nextInt(100000, 999999));
    return reactiveRedisTemplate.opsForValue()
        .set("_verify_code_" + email, verifyCode, Duration.ofMinutes(10))
        .then(Mono.fromCallable(() -> {
          long start = System.currentTimeMillis();
          String subject = "邮箱验证码";
          MimeMessage mailMessage = javaMailSender.createMimeMessage();
          MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true, "UTF-8");
          messageHelper.setFrom(sender, "如意");
          messageHelper.setSubject(subject);
          messageHelper.setTo(email);
          messageHelper.setText("您的验证码为： " + verifyCode);
          javaMailSender.send(mailMessage);
          return String
              .format("发送邮件 %s 成功, 耗时: %s s", subject, (System.currentTimeMillis() - start) / 1000);
        }));
  }

  @Override
  public Mono<Void> verifyEmailCode(String email, String emailCode) {
    return reactiveRedisTemplate.opsForValue().get("_verify_code_" + email)
        .filter(emailCode::equals)
        .switchIfEmpty(Mono.error(new LoginException("验证码不正确！")))
        .then();
  }

  @Override
  public Mono<String> login(String username, String password) {
    return userService.findUserByName(username).filter(user -> user.getPassword().equals(password))
        .switchIfEmpty(Mono.error(new LoginException("密码不符！")))
        .flatMap(user -> Mono
            .fromCallable(() -> {
              String urlParameters = String.format(
                  PASSWORD_FORMAT, kongAuthConfig.getGrantType(), kongAuthConfig.getClientId(),
                  kongAuthConfig.getClientSecret(), user.getName(), user.getPassword(),
                  user.getId(), user.getScopesString(), kongAuthConfig.getProvisionKey());
              byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
              return getToken(kongApiUrl + "/oauth2/token", kongPluginHost, trustManagers,
                  postData);
            }).doOnError(Throwable::printStackTrace));
  }

  private static String getToken(String urlStr,
      String kongPluginHost, TrustManager[] trustManagers, byte[] postData) throws Exception {

    URL url = new URL(urlStr);
    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("charset", "utf-8");
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
    connection.setRequestProperty("Host", kongPluginHost);
    connection.setDoOutput(true);

    SSLContext ctx = SSLContext.getInstance("TLS");
    ctx.init(null, trustManagers, null);
    connection.setSSLSocketFactory(ctx.getSocketFactory());
    connection.setHostnameVerifier((hostname, session) -> true);

    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
    wr.write(postData);
    wr.flush();
    wr.close();
    log.info(connection.getHeaderFields().toString());
    return getOutput(connection);
  }

  @Override
  public Mono<String> refreshToken(String refreshToken) {
    return Mono.fromCallable(() -> {
      String urlParameters = String
          .format(REFRESH_FORMAT, REFRESH_GRANT_TYPE, kongAuthConfig.getClientId(),
              kongAuthConfig.getClientSecret(), refreshToken);
      byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
      return getToken(kongApiUrl + "/oauth2/token", kongPluginHost, trustManagers, postData);
    });
  }

  private static String getOutput(HttpsURLConnection connection) throws IOException {
    InputStream inputStream =
        connection.getResponseCode() >= HttpStatus.BAD_REQUEST.value() ? connection.getErrorStream()
            : connection.getInputStream();
    BufferedReader read = new BufferedReader(new InputStreamReader(inputStream));
    String temp;
    StringBuilder re = new StringBuilder();
    while ((temp = read.readLine()) != null) {
      re.append(temp);
    }
    return re.toString();
  }
}
