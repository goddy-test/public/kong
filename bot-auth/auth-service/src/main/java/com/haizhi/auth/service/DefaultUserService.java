package com.haizhi.auth.service;

import com.haizhi.auth.dao.UserRepository;
import com.haizhi.auth.model.User;
import com.haizhi.auth.model.User.Scope;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/15.
 */
@Slf4j
@Service
public class DefaultUserService implements UserService {

  private final String[] DEFAULT_USER_SCOPE = {"read", "write"};

  private final List<Scope> scopeList = Arrays.asList(Scope.read, Scope.write);

  private final UserRepository userRepository;

  public DefaultUserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public Mono<User> findUserById(String id) {
    return userRepository.findById(id);
  }

  @Override
  public Mono<User> findUserByName(String name) {
    return userRepository.findByName(name);
  }

  @Override
  public Mono<User> createUser(User user) {
    return findUserByName(user.getName())
        .doOnNext(exist -> {
          throw new DuplicateKeyException("已存在该用户名:".concat(user.getName()));
        }).then(Mono.defer(() -> {
          user.setId(UUID.randomUUID().toString());
          Date now = new Date();
          user.setCreateTime(now);
          user.setUpdateTime(now);
          user.setScopes(scopeList);
          return userRepository.insert(user);
        }));
  }

  @Override
  public Mono<User> updateUser(User user) {
    return findUserByName(user.getName())
        .doOnNext(exist -> {
          if (!exist.getId().equals(user.getId())) {
            throw new DuplicateKeyException("用户名不存在！）");
          }
          user.setUpdateTime(new Date());
        }).then(userRepository.save(user));
  }
}
