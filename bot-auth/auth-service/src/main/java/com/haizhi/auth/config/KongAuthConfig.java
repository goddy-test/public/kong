package com.haizhi.auth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Goddy on 2018/11/19.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "kong.auth")
public class KongAuthConfig {

  private String clientId;
  private String clientSecret;
  private String provisionKey;
  private String grantType;
}
