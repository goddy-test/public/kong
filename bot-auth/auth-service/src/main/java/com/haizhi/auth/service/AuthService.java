package com.haizhi.auth.service;

import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/19.
 */
public interface AuthService {

  Mono<String> sendEmailCode(String email);

  Mono<Void> verifyEmailCode(String email, String emailCode);

  Mono<String> login(String username, String password);

  Mono<String> refreshToken(String refreshToken);
}
