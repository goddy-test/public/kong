package com.haizhi.auth.web;

import com.haizhi.auth.service.AuthService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/19.
 */
@RestController
public class AuthController {

  private final AuthService authService;

  public AuthController(AuthService authService) {
    this.authService = authService;
  }

  @GetMapping("/emailCode")
  public Mono<String> sendEmailCode(@RequestParam String email) {
    return authService.sendEmailCode(email);
  }

  @GetMapping("/login")
  public Mono<String> login(@RequestParam String username, @RequestParam String password) {
    return authService.login(username, password);
  }

  @GetMapping("/refreshToken")
  public Mono<String> refreshToken(@RequestParam String refreshToken) {
    return authService.refreshToken(refreshToken);
  }
}
