package com.haizhi.auth.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

/**
 * Created by Goddy on 2018/11/15.
 */
@SpringBootApplication(scanBasePackages = "com.haizhi")
@EnableReactiveMongoRepositories({"com.haizhi.auth.dao"})
public class Application {

  public static void main(String[] args) {
    System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
    SpringApplication.run(Application.class, args);
  }
}
