package com.haizhi.auth.service;

import com.haizhi.auth.model.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/15.
 */
@Service
public interface UserService {

  Mono<User> createUser(User user);

  Mono<User> findUserById(String id);

  Mono<User> findUserByName(String name);

  Mono<User> updateUser(User user);
}
