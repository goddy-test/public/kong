package com.haizhi.auth.web;

import com.google.common.base.Preconditions;
import com.haizhi.auth.model.User;
import com.haizhi.auth.service.AuthService;
import com.haizhi.auth.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/15.
 */
@RestController
@RequestMapping("/users")
public class UserController {

  private final UserService userService;

  private final AuthService authService;

  public UserController(UserService userService, AuthService authService) {
    this.userService = userService;
    this.authService = authService;
  }

  @PostMapping
  public Mono<User> createUser(@RequestBody User user, @RequestParam String emailCode) {
    return authService.verifyEmailCode(user.getEmail(), emailCode)
        .then(userService.createUser(user));
  }

  @PostMapping("/{userId}")
  public Mono<User> updateUser(@PathVariable String userId, @RequestBody User user,
      @RequestParam String emailCode) {
    Preconditions.checkArgument(userId.equals(user.getId()), "用户id不符！");
    return authService.verifyEmailCode(user.getEmail(), emailCode)
        .then(userService.updateUser(user));
  }
}
