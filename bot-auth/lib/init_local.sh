

function install_module() {
  groupId=$1
  artifactId=$2
  version=$3

  mvn dependency:purge-local-repository -DmanualInclude="${groupId}:${artifactId}"
  mvn install:install-file -Dfile=lib/${artifactId}-${version}.jar  -DgroupId=${groupId} -DartifactId=${artifactId} -Dversion=${version} -Dpackaging=jar
}

install_module "com.haizhi" "auth-model" "0.0.1-SNAPSHOT"
install_module "com.haizhi" "auth-db-access" "0.0.1-SNAPSHOT"
install_module "com.haizhi" "bot-common" "0.0.1-SNAPSHOT"
