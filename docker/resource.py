#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Goddy <wuchuansheng@yeah.net> 2018/11/5
# Desc: 

from flask import Flask, request, make_response
from flask_cors import *
import requests

app = Flask(__name__)
CORS(app, supports_credentials=True)


@app.route("/")
def test1():
    return "hi,goddy"


@app.route("/test")
def test2():
    return "test"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
