package com.goddy.demoresource;

import java.security.Principal;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/23.
 */
@RestController
public class TestController {

  @GetMapping("/1")
  @PreAuthorize("hasAuthority('read')")
  public Mono<String> auth1() {
    return Mono.just("hello, 1");
  }

  @PostMapping("/1")
  @PreAuthorize("hasAuthority('write')")
  public Mono<String> auth11(Principal principal) {
    return Mono.just(principal.getName());
  }

  @GetMapping("/2")
  @PreAuthorize("hasAuthority('delete')")
  public Mono<String> auth2() {
    return Mono.just("cool");
  }
}
