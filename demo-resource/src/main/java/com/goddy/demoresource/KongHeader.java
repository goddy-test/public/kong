package com.goddy.demoresource;

/**
 * Created by Goddy on 2018/11/23.
 */
public class KongHeader {
  public static final String clientId = "X-Consumer-ID";
  public static final String scopes = "X-Authenticated-Scope";
  public static final String userId = "X-Authenticated-Userid";
}
