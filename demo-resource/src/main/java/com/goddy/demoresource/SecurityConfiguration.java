package com.goddy.demoresource;

import java.security.Principal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * Created by Goddy on 2018/11/23.
 */
@EnableWebFluxSecurity
@Configuration
@EnableReactiveMethodSecurity
public class SecurityConfiguration {

  private class KongAuthenticationManager implements ReactiveAuthenticationManager {
    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
      return Mono.just(authentication);
    }
  }

  private class KongServerSecurityContextRepository implements ServerSecurityContextRepository {
    @Override
    public Mono<Void> save(ServerWebExchange serverWebExchange, SecurityContext securityContext) {
      // Do nothing...
      return null;
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange serverWebExchange) {
      String scopes = serverWebExchange.getRequest().getHeaders().getFirst(KongHeader.scopes);
      String userId = serverWebExchange.getRequest().getHeaders().getFirst(KongHeader.userId);
      Principal principal = () -> userId;
      Authentication authentication = new AnonymousAuthenticationToken("authenticated-user", principal,
          AuthorityUtils.createAuthorityList(scopes != null ? scopes.split(" ") : new String[0]));
      return Mono.just(new SecurityContextImpl(authentication));
    }
  }

  private final ReactiveAuthenticationManager authenticationManager;

  private final ServerSecurityContextRepository securityContextRepository;

  public SecurityConfiguration() {
    this.authenticationManager = new KongAuthenticationManager();
    this.securityContextRepository = new KongServerSecurityContextRepository();
  }

  @Bean
  public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {

    // Disable default security.
    http.httpBasic().disable();
    http.formLogin().disable();
    http.csrf().disable();
    http.logout().disable();

    // Add custom security.
    http.authenticationManager(this.authenticationManager);
    http.securityContextRepository(this.securityContextRepository);

    // Disable authentication for `/auth/**` routes.
//    http.authorizeExchange().pathMatchers("/auth/**").permitAll();
//    http.authorizeExchange().anyExchange().authenticated();

    http.authorizeExchange()
        .pathMatchers(HttpMethod.POST, "/1").hasAuthority("read")
        .pathMatchers("/**").authenticated();
    return http.build();
  }
}